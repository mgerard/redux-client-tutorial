import React from 'react/addons';
import Voting from '../../src/components/Voting';
import {expect} from 'chai';

const {renderIntoDocument, scryRenderedDOMComponentsWithTag, Simulate} = React.addons.TestUtils;

describe('Voting', () =>{

  it('renders a pair of buttons', () => {
    const component = renderIntoDocument(
      <Voting pair={["Map1", "Map2"]} />
    );
    const buttons = scryRenderedDOMComponentsWithTag(component, 'button');

    expect(buttons.length).to.equal(2);
    expect(buttons[0].textConect).to.equal('Map1');
    expect(buttons[1].textConect).to.equal('Map2');
  });

  it('invokes callback when a button is cicked', () =>{
    let votedWith;
    const vote = (entry) => votedWith = entry;

    const component = renderIntoDocument(
      <Voting pair={["Map1", "Map2"]} 
              vote={vote} />);
    const buttons = scryRenderedDOMComponentsWithTag(component, 'button');
    Simulate.click(buttons[0]);

    expect(votedWith).to.equal('Map1');
  });

  it('disables buttons when user has voted', () => {
    const component = renderIntoDocument(
      <Voting pair={["Map1","Map2"]}
              hasVoted="Map1" />
    );
    const buttons = scryRenderedDOMComponentsWithTag(component, 'button');

    expect (buttons.length).to.equal(2);
    expect (buttons[0].hasAttribute('disabled')).to.equal(true);
    expect (buttons[1].hasAttribute('disabled')).to.equal(true);
  });

  it('adds label to the voted entry', () =>{
    const component = renderIntoDocument(
      <Voting pair={["Map1","Map2"]}
              hasVoted="Map1" />
    );
    const buttons = scryRenderedDOMComponentsWithTag(component, 'button');

    expect(buttons[0].textConect).to.contain('Voted');
  });

  it('renders just the winner when there is one', () => {
    const component = renderIntoDocument(
      <Voting winner="Map1" />
    );
    const buttons = scryRenderedDOMComponentsWithTag(component, 'button');
    expect(buttons.length).to.equal(0);

    const winner = React.findDOMNode(component.refs.winner);
    expect(winner).to.be.ok;
    expect(winner.textContent).to.contain('Map1');
  });

});