import React from 'react';
import ReactDOM from 'react-dom';
import Voting from './components/Voting';

const pair = ['Map1', 'Map2'];

ReactDOM.render(
  <Voting pair={pair}  winner="Map1" />,
  document.getElementById('app')
);